package at.sonorys.svp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import at.sonorys.svp.framework.FrameworkConfiguration;


/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author pankaj
 *
 */

//<ApplicationName>sample_1</ApplicationName>	
//<ORBInitialHost>svp651</ORBInitialHost>	<!-- NameServer host name -->
//<ORBInitialPort>2809</ORBInitialPort>
//<ContextName>SVP</ContextName>
//<MpCcManager>MPCCM</MpCcManager>
//<UiManager>UIM</UiManager> 
//<ServiceNumber>1500*</ServiceNumber>
//<DebugLevel>5</DebugLevel>
//<MaxApplications>480</MaxApplications>
//<LogDir>/var/opt/svp/sample/log1</LogDir>
//<MaxLogFileSize>10</MaxLogFileSize> <!-- in MB -->
//<StatDir>/var/opt/svp/statistics</StatDir>
//<StatFileName>sample</StatFileName>
//<PurgeLogFilesInDays>10</PurgeLogFilesInDays>
//<PurgeStatFilesInDays>30</PurgeStatFilesInDays>
//<PurgingHour>1</PurgingHour>
//<CheckStatusTO>10</CheckStatusTO>
//<CommPort>24900</CommPort>
//<TracingPorts>49000</TracingPorts>
//<ValidateConfFiles>Yes</ValidateConfFiles>


@Entity
@Table(name="applications")
public class AppConfig {


	@Id
	@GeneratedValue
	private Integer id;
	private String applicationName;
	private String applicationRootDirectory;
	private String orbInitialHost;
	private Integer orbInitialPort;
	private String contextName;
	private String mpCcManager;
	private String uiManager;
	private String serviceNumber;
	private String debugLevel;
	private Integer maxApplications;
	private String logDir;
	private Integer maxLogfileSize;
	private Integer maxStatfileSize;
	private Integer maxStatFileWritePeriod;
	private Boolean statisticsWriteEpochTime;
	private String statDir;
	private String statFilename;
	private Integer purgeLogfilesInDays;
	private Integer purgeStatfilesInDays;
	private Integer purgingHour;
	private Integer checkStatusTo;
	private Integer commPort;
	private Boolean validateConfFiles;
	private String snmpHost;
	private Integer snmpPort;
	private Boolean assembleCallflowFile;
	private Boolean saveOldCallflowFile;
	private String callflowFileList;
	private String machineName;
	private String applicationData;

	
	public String toString() {
		return "id("+id+") applicationName("+applicationName
				+") applicationRootDirectory("+applicationRootDirectory
				+") orbInitialHost("+orbInitialHost
				+") orbInitialPort("+orbInitialPort
				+") contextName("+contextName
				+") mpCcManager("+mpCcManager
				+") uiManager("+uiManager
				+") serviceNumber("+serviceNumber
				+") debugLevel("+debugLevel
				+") maxApplications("+maxApplications
				+") logDir("+logDir
				+") maxLogfileSize("+maxLogfileSize
				+") maxStatfileSize("+maxStatfileSize
				+") maxStatFileWritePeriod("+maxStatFileWritePeriod
				+") statisticsWriteEpochTime("+statisticsWriteEpochTime
				+") statDir("+statDir
				+") statFilename("+statFilename
				+") purgeLogfilesInDays("+purgeLogfilesInDays
				+") purgeStatfilesInDays("+purgeStatfilesInDays
				+") purgingHour("+purgingHour
				+") checkStatusTo("+checkStatusTo
				+") commPort("+commPort
				+") validateConfFiles("+validateConfFiles
				+") snmpHost("+snmpHost
				+") snmpPort("+snmpPort
				+") assembleCallflowFile("+assembleCallflowFile
				+") saveOldCallflowFile("+saveOldCallflowFile
				+") callflowFileList("+callflowFileList
				+") machineName("+machineName
				+") applicationData("+applicationData
				+")";
	}

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	
	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getApplicationRootDirectory() {
		return applicationRootDirectory;
	}

	public void setApplicationRootDirectory(String applicationRootDirectory) {
		this.applicationRootDirectory = applicationRootDirectory;
	}

	public String getOrbInitialHost() {
		return orbInitialHost;
	}

	public void setOrbInitialHost(String orbInitialHost) {
		this.orbInitialHost = orbInitialHost;
	}

	public Integer getOrbInitialPort() {
		return orbInitialPort;
	}

	public void setOrbInitialPort(Integer orbInitialPort) {
		this.orbInitialPort = orbInitialPort;
	}

	public String getContextName() {
		return contextName;
	}

	public void setContextName(String contextName) {
		this.contextName = contextName;
	}
	
	public String getMpCcManager() {
		return mpCcManager;
	}

	public void setMpCcManager(String mpCcManager) {
		this.mpCcManager = mpCcManager;
	}

	public String getUiManager() {
		return uiManager;
	}

	public void setUiManager(String uiManager) {
		this.uiManager = uiManager;
	}

	public String getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public String getDebugLevel() {
		return debugLevel;
	}

	public void setDebugLevel(String debugLevel) {
		this.debugLevel = debugLevel;
	}

	public Integer getMaxApplications() {
		return maxApplications;
	}

	public void setMaxApplications(Integer maxApplications) {
		this.maxApplications = maxApplications;
	}

	public String getLogDir() {
		return logDir;
	}

	public void setLogDir(String logDir) {
		this.logDir = logDir;
	}

	public Integer getMaxLogfileSize() {
		return maxLogfileSize;
	}

	public Integer getMaxStatfileSize() {
		return maxStatfileSize;
	}

	public void setMaxStatfileSize(Integer maxStatfileSize) {
		this.maxStatfileSize = maxStatfileSize;
	}

	public Integer getMaxStatFileWritePeriod() {
		return maxStatFileWritePeriod;
	}

	public void setMaxStatFileWritePeriod(Integer maxStatFileWritePeriod) {
		this.maxStatFileWritePeriod = maxStatFileWritePeriod;
	}

	public Boolean getStatisticsWriteEpochTime() {
		return statisticsWriteEpochTime;
	}

	public void setStatisticsWriteEpochTime(Boolean statisticsWriteEpochTime) {
		this.statisticsWriteEpochTime = statisticsWriteEpochTime;
	}

	public void setMaxLogfileSize(Integer maxLogfileSize) {
		this.maxLogfileSize = maxLogfileSize;
	}

	public String getStatDir() {
		return statDir;
	}

	public void setStatDir(String statDir) {
		this.statDir = statDir;
	}

	public String getStatFilename() {
		return statFilename;
	}

	public void setStatFilename(String statFilename) {
		this.statFilename = statFilename;
	}

	public Integer getPurgeLogfilesInDays() {
		return purgeLogfilesInDays;
	}

	public void setPurgeLogfilesInDays(Integer purgeLogfilesInDays) {
		this.purgeLogfilesInDays = purgeLogfilesInDays;
	}

	public Integer getPurgeStatfilesInDays() {
		return purgeStatfilesInDays;
	}

	public void setPurgeStatfilesInDays(Integer purgeStatfilesInDays) {
		this.purgeStatfilesInDays = purgeStatfilesInDays;
	}

	public Integer getPurgingHour() {
		return purgingHour;
	}

	public void setPurgingHour(Integer purgingHour) {
		this.purgingHour = purgingHour;
	}

	public Integer getCheckStatusTo() {
		return checkStatusTo;
	}

	public void setCheckStatusTo(Integer checkStatusTo) {
		this.checkStatusTo = checkStatusTo;
	}

	public Integer getCommPort() {
		return commPort;
	}

	public void setCommPort(Integer commPort) {
		this.commPort = commPort;
	}

	public Boolean getValidateConfFiles() {
		return validateConfFiles;
	}

	public void setValidateConfFiles(Boolean validateConfFiles) {
		this.validateConfFiles = validateConfFiles;
	}

	public String getSnmpHost() {
		return snmpHost;
	}

	public void setSnmpHost(String snmpHost) {
		this.snmpHost = snmpHost;
	}

	public Integer getSnmpPort() {
		return snmpPort;
	}

	public void setSnmpPort(Integer snmpPort) {
		this.snmpPort = snmpPort;
	}

	public Boolean getAssembleCallflowFile() {
		return assembleCallflowFile;
	}

	public void setAssembleCallflowFile(Boolean assembleCallflowFile) {
		this.assembleCallflowFile = assembleCallflowFile;
	}

	public Boolean getSaveOldCallflowFile() {
		return saveOldCallflowFile;
	}

	public void setSaveOldCallflowFile(Boolean saveOldCallflowFile) {
		this.saveOldCallflowFile = saveOldCallflowFile;
	}


	public String getCallflowFileList() {
		return callflowFileList;
	}

	public void setCallflowFileList(String callflowFileList) {
		this.callflowFileList = callflowFileList;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public String getApplicationData() {
		return applicationData;
	}

	public void setApplicationData(String applicationData) {
		this.applicationData = applicationData;
	}

	public void setDefaultConfig(String applicationName) {
		setApplicationName(applicationName);
		setApplicationRootDirectory("/opt/svp/"+applicationName+"/");
		setOrbInitialHost("svp651");
		setOrbInitialPort(2809);
		setContextName("SVP");
		setMpCcManager("MPCCM");
		setUiManager("UIM");
		setServiceNumber("1500*");
		setDebugLevel("5");
		setMaxApplications(480);
		setLogDir("/var/opt/svp/"+applicationName+"/log1");
		setMaxLogfileSize(10);
		setMaxStatfileSize(10);
		setMaxStatFileWritePeriod(10);
		setStatisticsWriteEpochTime(false);
		setStatDir("/var/opt/svp/statistics");
		setStatFilename(applicationName);
		setPurgeLogfilesInDays(10);
		setPurgeStatfilesInDays(30);
		setPurgingHour(1);
		setCheckStatusTo(30);
		setCommPort(24999);
		setValidateConfFiles(true);
		setSnmpHost("dalton");
		setSnmpPort(161);
		setAssembleCallflowFile(true);
		setSaveOldCallflowFile(true);
		setCallflowFileList("/opt/svp/"+applicationName+"/config/callflowFileList.xml");
		setMachineName("");
		setApplicationData("/opt/svp/"+applicationName+"/config/"+applicationName+".xml");
	}

	public FrameworkConfiguration getFrameworkConfiguration() {
		FrameworkConfiguration frameworkConfiguration = new FrameworkConfiguration();
		frameworkConfiguration.setApplicationName(applicationName);
		frameworkConfiguration.setApplicationRootDirectory(applicationRootDirectory);
		frameworkConfiguration.setOrbInitialHost(orbInitialHost);
		frameworkConfiguration.setOrbInitialPort(orbInitialPort);
		frameworkConfiguration.setContextName(contextName);
		frameworkConfiguration.setMpCcManager(mpCcManager);
		frameworkConfiguration.setUiManager(uiManager);
		frameworkConfiguration.setServiceNumber(serviceNumber);
		frameworkConfiguration.setDebugLevel(debugLevel);
		frameworkConfiguration.setMaxApplications(maxApplications);
		frameworkConfiguration.setLogDir(logDir);
		frameworkConfiguration.setMaxLogfileSize(maxLogfileSize);
		frameworkConfiguration.setMaxStatfileSize(maxStatfileSize);
		frameworkConfiguration.setMaxStatFileWritePeriod(maxStatFileWritePeriod);
		frameworkConfiguration.setStatisticsWriteEpochTime(statisticsWriteEpochTime);		
		frameworkConfiguration.setStatDir(statDir);
		frameworkConfiguration.setStatFilename(statFilename);
		frameworkConfiguration.setPurgeLogfilesInDays(purgeLogfilesInDays);
		frameworkConfiguration.setPurgeStatfilesInDays(purgeStatfilesInDays);
		frameworkConfiguration.setPurgingHour(purgingHour);
		frameworkConfiguration.setCheckStatusTo(checkStatusTo);
		frameworkConfiguration.setCommPort(commPort);
		frameworkConfiguration.setValidateConfFiles(validateConfFiles);
		frameworkConfiguration.setSnmpHost(snmpHost);
		frameworkConfiguration.setSnmpPort(snmpPort);
		frameworkConfiguration.setAssembleCallflowFile(assembleCallflowFile);
		frameworkConfiguration.setSaveOldCallflowFile(saveOldCallflowFile);
		frameworkConfiguration.setCallflowFileList(callflowFileList);
		frameworkConfiguration.setMachineName(machineName);
		frameworkConfiguration.setApplicationData(applicationData);
		return frameworkConfiguration;
	}
	
}
