package at.sonorys.svp.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import at.sonorys.svp.service.ApplicationFrameworkService.Day;
import at.sonorys.svp.service.ApplicationFrameworkService.Month;


//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author pankaj
 *
 */


@Entity
//@Table(name="\"CDR\"")
public class Cdr {



	@Id
//	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Integer id;
	
//	@Column(name="\"MSISDN\"")
	private String msisdn;

//	@Column(name="misc")
	private String misc;

	@Enumerated(EnumType.ORDINAL)
	private Day day;

	@Enumerated(EnumType.ORDINAL)
	private Month month;


	public Month getMonth() {
		return month;
	}

	public void setMonth(Month month) {
		this.month = month;
	}

	public Day getDay() {
		return day;
	}

	public void setDay(Day day) {
		this.day = day;
	}
	

	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
//	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> phoneNumbers = new ArrayList<String>();
	  
	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
    private List<Enum> enums = new ArrayList<Enum>();
	  

	public List<Enum> getEnums() {
		return enums;
	}

	public void setEnums(List<Enum> enums) {
		this.enums = enums;
	}

	public List<String> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

//	public List<Enum> getEnums() {
//		String enumlist = enums; // 1,2,5,3,2,
//		List<Enum> enumArrayList = new ArrayList<Enum>(); 
//		int	pos1 = enumlist.indexOf(",");
//		String enumx = enumlist.substring(0, pos1);
//		Day dayx = Day.valueOf(enumx);
//				enumlist = enumlist.substring(pos1+1);
//			
//				enumArrayList.add(Enum.
//			}
//			
//		return enums;
//	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getMisc() {
		return misc;
	}

	public void setMisc(String misc) {
		this.misc = misc;
	}
	
}
