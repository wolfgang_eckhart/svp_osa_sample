package at.sonorys.svp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import at.sonorys.svp.framework.FrameworkConfiguration;


/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author pankaj
 *
 */


@Entity
@Table(name="sample_variables")
public class AppVariable {


	@Id
	@GeneratedValue
	private Integer id;

	private String name;
	private String value;

	
	public String toString() {
		return "id("+id+") name("+name+") value("+value+")";
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}
	
	
}
