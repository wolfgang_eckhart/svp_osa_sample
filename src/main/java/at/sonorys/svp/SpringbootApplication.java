package at.sonorys.svp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@SpringBootApplication(exclude = {EmbeddedServletContainerAutoConfiguration.class,
//        WebMvcAutoConfiguration.class})
public class SpringbootApplication {

	public static void main(String[] args) {
		System.out.println("Pass arguments to SpringbootApplication...");
		SpringApplication.run(SpringbootApplication.class, args);
	}
	

}
