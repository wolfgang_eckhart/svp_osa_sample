/*
 * Application.java
 * 
 * Created on 16.10.2003
 *
 * Sonorys Technology GesmbH
 * Always ready for Conversation
 * 
 */
package at.sonorys.svp.application;

import at.sonorys.svp.entity.Cdr;
import at.sonorys.svp.service.ApplicationFrameworkService;
import at.sonorys.svp.service.impl.CdrService;
import at.sonorys.util.Tools;
import java.util.BitSet;
import java.util.Vector;
import org.csapi.TpAddress;
import org.csapi.cc.TpCallAppInfo;
import org.springframework.beans.factory.annotation.Autowired;



/**
 * Object of this class represents the "Application-Object". 
 */
public class Application extends ApplImpl{
	
	@Autowired
	private CdrService cdrService;

	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// 	
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	private ApplPersData apd = null;
	

	/** Creates a new instance of Application */
	public Application() {
	}

	/**
	 * Method will be called at the end of init() method of class ApplImpl.
	 * Example added to create a instance of ApplicationPersistentDataObject ApplPersData()
	 */
	public void applSpecificInit(){ 
		try{          
			trace(5,"applSpecificInit - sample - begin"); 
            System.out.println("Application.applSpecificInit - sample - begin...");
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// init Variables  
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			if (((ApplicationFrameworkService)afws).getSystemVariable("FakeIncomingCli").length()>0){
				trace(5,"applSpecificInit - FakeIncomingCli("+afws.getVariable("FakeIncomingCli")+")");
				cli = ((ApplicationFrameworkService)afws).getSystemVariable("FakeIncomingCli");
			}
			apd = new ApplPersData();
			String useAsr = this.getVariable("AsrSwitchedOn");
			if (useAsr.equals("OFF")){
				trace(5,"applSpecificInit - AsrSwitchedOn is set to 'OFF' !!!!!!!");
				this.setAsrSwitchedOn(false);
			}else{
				trace(5,"applSpecificInit - AsrSwitchedOn is set to 'ON' !!!!!!!");
				this.setAsrSwitchedOn(true);
			}
			trace(3,"applSpecificInit - create CDR"); 
			Cdr cdr = new Cdr();
			cdr.setMsisdn(cli);
			cdr.setMisc("Application("+Tools.getTime()+")");
//			cdrService.persistCdr(cdr);
			
		} catch (Exception e) {
			trace(2,"Exception in Application.applSpecificInit:  " + e) ;
			e.printStackTrace(System.out);
		}
	}

	/**
	 * Method will be called before first Application-block start's.
	 * It's the right place e.g.: to start application specific tasks.
	 * Use public field afws for callback methods to ApplicationFrameworkService.
	 */
	public void started(){
		try{
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// Read redirecting Number  
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++			
		    getApd().setRedirectingNr("");
		    if (this.simMode==false){
				TpCallAppInfo appInfo = notificationInfo.CallAppInfo[0];
				if (appInfo==null){
					trace(5,"started - notificationInfo.CallAppInfo[0].CallAppRedirectingAddress()==null!!!");
				}else{
					TpAddress redirNr = null;
				    try{
				        redirNr = appInfo.CallAppRedirectingAddress();
					} catch (Exception e) {
						trace(2,"started - Exception at appInfo.CallAppRedirectingAddress(): "+e);
					}
					if (redirNr==null){
						trace(5,"started - notificationInfo.CallAppInfo[0].CallAppRedirectingAddress()==null!!!");
					}else{
					    getApd().setRedirectingNr(redirNr.AddrString);
					}
				}
		    }
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// Example for using ApplicationFrameworkService - callback (afws) 
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++			
			String result = afws.serviceCallBack("Application("+nr+") started");    
			trace(5,"started ("+result+")");
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// Example to create an Statistic entry  
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++			
			generateStatisticEntry("Appl("+nr+")Numbers:CLI("+cli+")CalledNr("+getCalledNumber()+")RedirNr("+getApd().getRedirectingNr()+")","");
		}catch (Exception e) {
			trace(2,"Exception at Application.started: "+e);
		}
	}
 
	/**
	 * Method will be called before application-object shut's down.
	 * it's the right place to cleanup
	 */
	public void shutdown(){ 
		try{
			trace(5,"shutdown - begin");
		}catch (Exception e) {
			trace(2,"Exception at Application.shutdown: "+e);
		}
	}

 

	/**
	 * Method returns application-persistent Data object
	 */
	public ApplPersData getApd() {
		return apd;
	}

	/**
	 * Example, to fill dynamic Prompt of Block
	 */
	public synchronized void fillDynamicPrompt(Vector promptList, String placeholder, String dynContent){
		try{  
			/**        
			 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			 *	Method will be used, to replace an Prompt-placeholder, by the dynamic 
			 *  Prompt Content
			 *
			 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			 */
			trace(5,"fillDynamicPrompt("+promptList.toString()+","+placeholder+","+dynContent+")");
			int size = promptList.size();
			if (size<=0){
				return;
			}
			for (int i=0;i<size;i++){
				String promptContent = (String)promptList.get(i);
				promptContent = promptContent.replaceAll(placeholder,dynContent);
				promptList.setElementAt(promptContent,i);
				if (promptContent.equals("")){
					promptList.removeElementAt(i);
					i--;
					size--;
				}
			}
			trace(5,"fillDynamicPrompt - new List("+promptList.toString()+")");
		} catch (Exception ex) {
			trace(2,"fillDynamicPrompt - Exception: "+ex);
			ex.printStackTrace(System.out);
		}
	} 

	public int getNrApplInstances(){
		try{
			trace(5,"getNrApplInstances - begin");
			BitSet applBitSet = afw.getApplBitSet();
			String applBits = applBitSet.toString();
			int size = applBitSet.size();
			int used = 0;
			for (int i=0;i<size;i++){
				if (applBitSet.get(i)){
					used++;
				}
			}
			trace(5,"getNrApplInstances("+applBits+")");
			trace(5,"getNrApplInstances used("+used+")");
			return used;
		}catch (Exception e) {
			trace(2,"getNrApplInstances - Exception: "+e);
			return -1;
		}
	}


}
