/*
 * PlayVoiceXxx.java
 * 
 * Created on 11.05.2004
 *
 * Sonorys Technology GesmbH
 * Always ready for Conversation
 * 
 */
package at.sonorys.svp.application.start;

import at.sonorys.svp.application.*;
import at.sonorys.util.Tools;

/**
 * @author Wolfgang Eckhart
 */



/**
 * Example Class for Enhanced PlayVoice Block. 
 * !!! INSERT HERE ALL METHODS THAT HAVE TO BE OVERWRITTEN FOR THIS EXPLICIT BLOCK !!!
 * ---> Use this file to create your own PlayVoiceBlockClasses.
 * (Xxx has to be the name of PlayVoice Block in XML-file.
 * Example: PlayVoice Block with name "Test"  -> create Class "PlayVoiceTest").
 */

public class PlayVoiceWelcome extends PlayVoice{
	
	
	/** Creates a new instance of PlayVoiceXxx */
	public PlayVoiceWelcome() {
	}

       
	/**
	 * Will be called at the end of init() method of class VoiceBlockImpl.
	 * It's the right place here e.g.: to change the prompts dynamically.
	 */
	public void applSpecificInit(){
		try{          
			/**
			 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			 *	All specific initialization for the Block will be done in this methode
			 *
			 *	Example to replace complete promptList:
			 *
			 *	promptList.removeAllElements();
			 *	promptList.add("ID_xxxx");
			 *
			 *	Example to fill "placeholders" in promptList:
			 *
			 *	use Methode fillDynamicPrompt from Application Object:
			 *	public synchronized void fillDynamicPrompt(Vector promptList, String placeholder, String dynContent){
			 *
			 *	Example:
			 *			app.fillDynamicPrompt(promptList,"%neueMitteilungEN","M_MitteilungenNurNeue_p4");
			 *
			 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			 */
			trace(5,"applSpecificInit - begin ...");
		} catch (Exception ex) {
			trace(2,"applSpecificInit - Exception: "+ex);
			ex.printStackTrace(System.out);
			checkNextAction(PlayVoiceActionDefinition.OK,prevResult,legNr); 
			return;
		}
	} 

	/**
	 * Will be called before Block-Action OK will be called
	 */
	public void ok() {
		trace(5,"ok: checkCalledNumber("+app.getCalledNumber()+")");
		try{ 
			trace(5,"ok: ServiceNumberRefApp("+app.afws.getVariable("ServiceNumberRefApp")+")");
			if (app.getCalledNumber().equals(app.afws.getVariable("ServiceNumberRefApp"))){
				trace(5,"ok: serviceNumber for RefApp called - call Result1...");
				checkNextAction(PlayVoiceActionDefinition.Result1,prevResult,legNr); 
				return;
			}
			checkNextAction(PlayVoiceActionDefinition.OK,prevResult,legNr); 
			return;
		} catch (Exception ex) {
			trace(2,"Exception in .ok: "+ex);
			ex.printStackTrace(System.out);
		}
	}
    
    
	/**
	 * Will be called before Block-Action Error will be called
	 */
	public void error() {
		trace(5,"error: begin");
		try{ 
			// will be overwritten in child class
		} catch (Exception ex) {
			trace(2,"Exception in .error: "+ex);
			ex.printStackTrace(System.out);
		}
	}

    
}




