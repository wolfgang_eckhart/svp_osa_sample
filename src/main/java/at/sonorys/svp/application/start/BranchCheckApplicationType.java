/*
 * BranchXxx.java
 * 
 * Created on 11.05.2004
 *
 * Sonorys Technology GesmbH
 * Always ready for Conversation
 * 
 */
package at.sonorys.svp.application.start;

import at.sonorys.svp.application.Branch;
import at.sonorys.svp.application.BranchActionDefinition;
import at.sonorys.svp.entity.Cdr;
import at.sonorys.svp.service.impl.CdrService;
import at.sonorys.util.Tools;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Wolfgang Eckhart
 */

/**
 * Example Class for Enhanced Branch Block. 
 * !!! INSERT HERE ALL METHODS THAT HAVE TO BE OVERWRITTEN FOR THIS EXPLICIT BLOCK !!!
 * ---> Use this file to create your own BranchBlockClasses.
 * (Xxx has to be the name of Branch Block in XML-file.
 * Example: Branch Block with name "Test"  -> create Class "BranchTest").
 */
public class BranchCheckApplicationType extends Branch{
    
	@Autowired
	private CdrService cdrService;

	/** Creates a new instance of BranchXxx */
	public BranchCheckApplicationType(){
	}
	


	/**
	 * Will be called at the end of init() method of class BlockImpl.
	 * !!! OVERWRITES METHOD applSpecificInit() OF CLASS "Branch".
	 */
	public void applSpecificInit(){
		try{ 
			trace(5,"applSpecificInit - this is the sample specific Branch...");
			Cdr cdr = new Cdr();
			cdr.setMsisdn("43664"+this.app.cli);
			cdr.setMisc("BranchCheckApplicationType("+Tools.getTime()+")");
//			cdrService.persistCdr(cdr);
		} catch (Exception ex) {
			trace(2,"applSpecificInit - Exception: "+ex);
			ex.printStackTrace(System.out);
		}
	} 
	
	/**
	 * Will be called, when branch-block has started.
	 * Insert here code for decisions
	 * Result1 is default action for Branch Block
	 */
	public void calculateResult(){
		try{          
			/**
			 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			 *	Example for child-Class to call checkNextAction:
			 *
			 *	this.checkNextAction(BranchActionDefinition.Result1,prevResult,legNr);  
			 *
			 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			 */
			trace(5,"calculateResult - check, if OSAX is set");
			if (Tools.isValueSet(app.afw.getSystemVariable("OSAX"))){
				trace(3,"calculateResult - SystemVariable\"OSAX\" is set -> call Result2");
				checkNextAction(BranchActionDefinition.Result2,prevResult,legNr); 
				return;
			}
			trace(3,"calculateResult - SystemVariable\"OSAX\" is NOT set -> call Result1");
			checkNextAction(BranchActionDefinition.Result1,prevResult,legNr); 
		} catch (Exception ex) {
			trace(2,"calculateResult - Exception: "+ex);
			ex.printStackTrace(System.out);
			checkNextAction(BranchActionDefinition.Result1,prevResult,legNr); 
		}
	}  
        
    
}




