package at.sonorys.svp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import at.sonorys.svp.framework.ApplicationFrameworkOperations;
import at.sonorys.svp.service.ApplicationFrameworkService;

@Controller
public class FrameworkController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ApplicationFrameworkOperations applicationFrameworkOperations;
	
	@Autowired
	private ApplicationFrameworkService applicationFrameworkService;
	
	@RequestMapping(value="/framework/attach", method = RequestMethod.GET)
	@ResponseBody
	public String attachFramework() {
		System.out.println("FrameworkController - attachFramework called...");
		applicationFrameworkService.attachFramework();
		return "success";
	}

	
	@RequestMapping(value="/framework/detach", method = RequestMethod.GET)
	@ResponseBody
	public String detachFramework() {
		System.out.println("FrameworkController - detachFramework called...");
		applicationFrameworkService.detachFramework();
		return "success";
	}

	
	@RequestMapping(value="/framework/shutdown", method = RequestMethod.GET)
	@ResponseBody
	public String shutdownFramework() {
		if (applicationFrameworkService.isShutdownEnabled()){
			//OK
		}else{
			System.out.println("FrameworkController - shutdown is disabled - return...");
			return "shutdown disabled over http...";
		}
		System.exit(applicationFrameworkService.getReturnValue());
		return "success";
	}

	
	@RequestMapping(value = "/framework/status", method = RequestMethod.GET)
	@ResponseBody
	public String getFrameworkStatus() {
		logger.info("getFrameworkStatus() called...");
		return applicationFrameworkService.getFrameworkStatus();
	}

	@RequestMapping(value = "/framework/act", method = RequestMethod.GET)
	@ResponseBody
	public String getFrameworkAct() {
		return applicationFrameworkService.getActiveApplicationInstances();
	}

	@RequestMapping(value = "/framework/info", method = RequestMethod.GET)
	@ResponseBody
	public String getFrameworkInfo() {
		return applicationFrameworkService.getFrameworkInfo();
	}

	@RequestMapping(value = "/framework/purge", method = RequestMethod.GET)
	@ResponseBody
	public String getFrameworkPurge() {
		return applicationFrameworkService.purgeFrameworkLogs();
	}

	@RequestMapping(value = "/framework/initext", method = RequestMethod.GET)
	@ResponseBody
	public String getFrameworkInitext() {
		applicationFrameworkService.initForExternalServices();
		return "initForExternalServices finished...";
	}

	@RequestMapping(value = "/framework/readcfg", method = RequestMethod.GET)
	@ResponseBody
	public String getFrameworkReadcfg() {
		return applicationFrameworkService.readFrameworkConfig();
	}

	@RequestMapping(value = "/framework/readall", method = RequestMethod.GET)
	@ResponseBody
	public String getFrameworkReadall() {
		return applicationFrameworkService.readFrameworkConfigAndCallflow();
	}


}
