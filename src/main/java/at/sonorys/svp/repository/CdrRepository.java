package at.sonorys.svp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import at.sonorys.svp.entity.Cdr;

public interface CdrRepository extends JpaRepository<Cdr, Integer>{

	List<Cdr> findByMisc(String misc);
	
}
