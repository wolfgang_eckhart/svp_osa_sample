package at.sonorys.svp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import at.sonorys.svp.entity.AppConfig;

public interface AppConfigRepository extends JpaRepository<AppConfig, Integer>{

	List<AppConfig> findByApplicationName(String applicationName);
	
}
