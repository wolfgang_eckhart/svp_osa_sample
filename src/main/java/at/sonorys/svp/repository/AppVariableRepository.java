package at.sonorys.svp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import at.sonorys.svp.entity.AppVariable;

public interface AppVariableRepository extends JpaRepository<AppVariable, Integer>{

	List<AppVariable> findByName(String name);
	
}
