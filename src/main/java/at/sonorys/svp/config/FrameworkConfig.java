package at.sonorys.svp.config;

import java.util.Hashtable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import at.sonorys.svp.entity.AppConfig;
import at.sonorys.svp.framework.ApplicationFramework;
import at.sonorys.svp.framework.FrameworkConfiguration;
import at.sonorys.svp.service.ApplicationFrameworkService;
import at.sonorys.svp.service.impl.AppConfigService;
import at.sonorys.svp.service.impl.ConfigService;

@Configuration
public class FrameworkConfig {

	
	@Value("${myname}")
	private String configFile;

	@Value("${applicationName}")
	private String applicationName;

	@Autowired
	private Environment environment;

	@Autowired
	private ConfigService configService;

	@Autowired
	private AppConfigService appService;

	
	@Bean(initMethod="run", destroyMethod="shutdown") 
	public ApplicationFramework getApplicationFramework(ApplicationFrameworkService applicationFrameworkService){
		
		System.out.println("Configfile (default): "+configFile);
		System.out.println("applicationName: "+applicationName);
		ApplicationFramework afw = null;
		FrameworkConfiguration frameworkConfiguration = prepareConfig();
		if (frameworkConfiguration==null){ 		//Config has been written to app_cfg1.xml
			System.out.println("ConfigFile defined("+configFile+") in properties file - use this file for config...");
			Hashtable<String, String> configVariables = configService.readConfigFile(configFile);
			afw = new ApplicationFramework(configVariables);
		}else{
			afw = new ApplicationFramework(frameworkConfiguration);
		}
		afw.setApplicationFrameworkService(applicationFrameworkService);
		return afw;		
	}
	
	
	@Bean
	public ApplicationFrameworkService getApplicationFrameworkService() {
		return new ApplicationFrameworkService();
	}
	
	public String getConfigFile() {
		return configFile;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public FrameworkConfiguration prepareConfig() {
		AppConfig appConfig = null;
		Hashtable<String, String> configVariables = configService.readConfigFile(configFile);
		String configFile = configVariables.get("ConfigFile");
		if (configFile!=null){ 		//Config defined in app_cfg1.xml
			System.out.println("ConfigFile defined("+configFile+") - use this file for config...");
			return null;
		}else{						//Config defined in database
			System.out.println("read config for application("+applicationName+") from db...");
			appConfig = appService.getAppConfig(applicationName);	
			FrameworkConfiguration frameworkConfiguration = appConfig.getFrameworkConfiguration();
			frameworkConfiguration.setVariables(configVariables);
			return frameworkConfiguration;
		}
	}

	
}
