package at.sonorys.svp.service.api;

import java.util.List;

import at.sonorys.svp.entity.AppVariable;

public interface AppVariableApi {

	public Integer persistAppVariable(AppVariable appVariable);
	
	public List<AppVariable> searchAppVariables();
	
	public AppVariable getAppVariable(String name);
	
}
