package at.sonorys.svp.service.api;

import java.util.List;

import at.sonorys.svp.entity.Cdr;

public interface CdrApi {

	public Integer persistCdr(Cdr cdr);
	
	public List<Cdr> searchCdrs();
	
	public Boolean deleteCdr(Integer id);

	
}
