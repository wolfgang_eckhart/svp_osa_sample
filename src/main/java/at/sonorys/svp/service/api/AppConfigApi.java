package at.sonorys.svp.service.api;

import java.util.List;

import at.sonorys.svp.entity.AppConfig;

public interface AppConfigApi {

	public Integer persistApp(AppConfig apps);
	
	public List<AppConfig> searchApps();
	
	public AppConfig getAppConfig(String applicationName);
	
}
