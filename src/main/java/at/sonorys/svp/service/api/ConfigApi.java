package at.sonorys.svp.service.api;

import java.util.Hashtable;

public interface ConfigApi {

	public Hashtable<String,String> readConfigFile(String filename);
	
	public int writeConfigFile(Hashtable<String,String> variables, String filename);
	
}
