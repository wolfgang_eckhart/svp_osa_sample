/*
 * ApplicationFrameworkService.java
 * 
 * Created on 16.10.2003
 *
 * Sonorys Technology GesmbH
 * Always ready for Conversation
 * 
 */

package at.sonorys.svp.service;


import at.sonorys.svp.application.CallParams;
import at.sonorys.svp.application.util.Tools;
import at.sonorys.svp.config.FrameworkConfig;
import at.sonorys.svp.controller.FrameworkController;
import at.sonorys.svp.entity.AppConfig;
import at.sonorys.svp.entity.Cdr;
import at.sonorys.svp.framework.ApplicationFramework;
import at.sonorys.svp.framework.FrameworkConfiguration;
import at.sonorys.svp.service.impl.AppConfigService;
import at.sonorys.svp.service.impl.CdrService;
import at.sonorys.svp.service.impl.ConfigService;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Hashtable;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author  eckhart
 */
public class ApplicationFrameworkService extends DefaultFrameworkService {  

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CdrService cdrService;
	@Autowired
	private AppConfigService appService;
	@Autowired
	private ConfigService configService;
	@Autowired
	private FrameworkController frameworkController;
	@Autowired
	private FrameworkConfig frameworkConfig;

	private Thread thread = null;
	static public ApplicationFramework afw = null;
	public String[] args = null;
	private SysPersData spd = null;
	public boolean startedUp = false;
	private int timeCtr = 0;
	private boolean registered = false;
	
	private boolean frameworkDetached = false;
		
	private boolean detachedManually = false;

	private int returnValue = 1;
	private boolean shutdownCalled = false;
	private boolean shutdownEnabled = false;

	
    public enum Day {
        SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
        THURSDAY, FRIDAY, SATURDAY 
    }
    
    public enum Month {
        January, February, March, April, May, June, July, August, September, October, November, December 
    }
    
    
	/** Creates a new instance of ApplicationFrameworkService */
	public ApplicationFrameworkService(String[] args) {
	  	this.args = args;
	}
    
    /** Creates a new instance of ApplicationFrameworkService */
	public ApplicationFrameworkService() {
	}
    
	public void setApplicationFramework(ApplicationFramework afw) {
        try{  
        	this.afw = afw;
        } catch (Exception ex) {
            trace(2,"Exception in .setApplicationFramework: "+ex);
            System.out.println("Exception in ApplicationFrameworkService.setApplicationFramework: "+ex);
        }
	}

	/**
     * @param args the command line arguments
     */
    
    
	/**Main method*/
//    public static void main(String[] args) {
//		ApplicationFrameworkService afws = new ApplicationFrameworkService(args);
//		afws.start();
//    }

	public void run() {
		System.out.println("ApplicationFrameworkService.run: begin...");
		init();
	} 

	public void init() {
		System.out.println("ApplicationFrameworkService.init: begin...");
		afw.version = afw.version+") \n"+VersionApp.version;
		spd = new SysPersData();
	} 

	/**start method*/
	public void start() {
		System.out.println("ApplicationFrameworkService.start - begin");
	  	thread = new Thread(this);
	  	thread.start();
	}
	
	/**
	 * call this method to shutdown the ApplicationFramework
	 */
	public void stop(){ 
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// 
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  		if (afw != null) {
			afw.stop();
  		}
  		System.exit(0); 
	}
	
	/**
	 * call this method to detach the ApplicationFramework, 
	 * e.g. if a essential service fails
	 */
	public void detachFramework() {
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// 
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		if (afw != null) {
		    frameworkDetached = true;
			afw.detach();
			trace(5,"ApplicationFrameworkService.detachFramework: ApplicationFramework detached");
		} 
	}
	
	/**
	 * call this method to re-attach the ApplicationFramework, 
	 * e.g. if a essential service is back again
	 */
	public void attachFramework() { 
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// 
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		if (afw != null) {
		    frameworkDetached = false;
			afw.attach();
			trace(5,"ApplicationFrameworkService.attachFramework: ApplicationFramework attached");
		}
	}
    

	/**
	 * Example to create an Outcall with method "createCall" from ApplicationFramework, 
	 * Parameter for outcall can be any type of Object
	 * This Parameter-Object will be stored in Application Object, and is returned by method
	 * app.getCreateCallParams() -> this method has to be used by MakeCallXxx Block to get CallParameters!!!
	 * 
	 * !!! THE FIRST BLOCK IN APPLICATION-XML FILE HAS TO BE "MakeCall" IN THIS CASE !!!
	 * 
	 * Here is an Example how to implement it to MakeCallXxx Block method applSpecificInit()
	 * 
	 * public void applSpecificInit(){ 
	 *		Object callParams = app.getCreateCallParams();
	 *		
	 *		if (callParams instanceof String) {
	 *			this.prevResult = (String)callParams;
	 *		}
	 *		if (callParams instanceof CallParams) {
	 *			CallParams cpx = (CallParams)callParams;
	 *			this.prevResult = cpx.getNumber();
	 *			this.callingLineId = cpx.getCli();
	 *		}
     *
	 */
	public void createCall(CallParams cp) { 
		try{
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// 
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			if (afw != null) {
				afw.createCall(cp);
				trace(5,"ApplicationFrameworkService.createCall: Application started");
			}
		}catch (Exception e){
			trace(2,"createCall - Exception: "+e);
		} 
	}
    

	/**
	 * This method is an callback-example (to be used by Application Objects)
	 * callback-methods have to be "synchronized" (to be used from several application Objects)
	 * Usage: e.g. for for service requests
	 * See also Application.java 
	 * Already Implemented callback: Message "ApplicationName:xxxxx" indicates an sucessful startup 
	 * of ApplicationFramework -> boolean startedUp =true;	
	 */
	public synchronized String serviceCallBack(String msg) { 
		try{
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// 
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			String result = "";
			if (msg.equals("CheckStatus")){
				if (detachedManually){
					trace(5,"serviceCallBack - CheckStatus - will be igrored, because Framework is detached manually!!!");	
				}else{
					checkStatus();					
				}
				return("ApplicationFrameworkService.serviceCallBack");
			}
			trace(5,"serviceCallBack ("+msg.toString()+")");
			if (msg.equals("Shutdown")){
				trace(5,"serviceCallBack - System Shutdown!!!");	
				shutdown();
			}
			if (msg.equals("Status")){
				trace(5,"serviceCallBack - Status");	
				result = 
						"Sample Application O.K...."
						+ ")" + '\r' + '\n';
//				Cdr cdr = new Cdr();
//				cdr.setMsisdn("0043");
//				cdr.setMisc("Sample Application O.K....("+Tools.getTime()+")");
//				cdrService.persistCdr(cdr);
//				AppConfig app = new AppConfig();
//				app.setApplicationName("Samplex");
//				app.setOrbInitialHost("svp651");
//				appService.persistApp(app);
//				List<AppConfig> apps = appService.searchApps();
//				int size = apps.size();
//				for (int i = 0;i<size;i++){
//					result = result+apps.get(i).toString()+ '\r' + '\n';
//				}
				return result;
			}
			if (msg.indexOf("ApplicationName:")==0){
				trace(5,"serviceCallBack - Startup FINISHED!!!");	
				startedUp=true;			
			}
			if (msg.equals("initextsrv")){
				trace(5,"serviceCallBack - Init external Services");	
				initForExternalServices();
			}
			if (msg.equals("cmd:shutdown")){ 
				trace(5,"serviceCallBack - shutdown");
				frameworkController.shutdownFramework();
			}
			if (msg.equals("cmd:createconfig")){ 
				trace(5,"serviceCallBack - createconfig");
				String applicationName = msg.substring(16);
				AppConfig appConfig = new AppConfig();
				appConfig.setDefaultConfig(applicationName);
				appService.persistApp(appConfig);
				List<AppConfig> apps = appService.searchApps();
				int size = apps.size();
				for (int i = 0;i<size;i++){
					result = result+apps.get(i).toString()+ '\r' + '\n';
				}
				return result;
			}
			if (msg.equals("cmd:createcdr")){ 
				List<String> phoneNumbers = new ArrayList<String>();
				phoneNumbers.add("436641951558");
				phoneNumbers.add("436641234567");
				Day daytest = Day.THURSDAY;
				System.out.println("daytest.ordinal: "+daytest.ordinal());
				System.out.println("daytest.name: "+daytest.name());
				System.out.println("daytest.toString: "+daytest.toString());
				Cdr cdr = new Cdr();
				cdr.setMisc("Test");
				cdr.setDay(Day.SUNDAY);
				cdr.setMonth(Month.February);
				cdr.setPhoneNumbers(phoneNumbers);
				List<Enum> enums = new ArrayList<Enum>();
				enums.add(Day.MONDAY);
				enums.add(Month.February);
				//cdr.setEnums(enums);
				cdr.setEnums(enums);
//				cdrService.persistCdr(cdr);
	
//
//				for(Enum enumx : enums){
//					System.out.println(enumx);
//				}

//				List<String> items = new ArrayList<String>();
//				items.add("A");
//				items.add("B");
//				items.add("C");
//				items.add("D");
//				items.add("E");
//
//				for(String item : items){
//					System.out.println(item);
//				}
				
//				List<Enum> miscenums = new ArrayList<Enum>(); 
//				miscenums.add(Day.SUNDAY);
//				miscenums.add(Day.WEDNESDAY);
//				miscenums.add(Month.February);
//				cdr.setMisc("Test2");
//				cdr.setEnums(miscenums);
//				cdrService.persistCdr(cdr);
				
//				MyParams myParams = new MyParams();
//				myParams.setDay(Day.SUNDAY);
//				myParams.setMonth(Month.February);
//				String myParamsSerialized = "";
//				
//				 // serialize the object
//				 try {
//				     ByteArrayOutputStream bo = new ByteArrayOutputStream();
//				     ObjectOutputStream so = new ObjectOutputStream(bo);
//				     so.writeObject(myParams);
//				     so.flush();
//				     myParamsSerialized = bo.toString();
//				 } catch (Exception e) {
//				     System.out.println(e);
//				 }
//
//				System.out.println("myParamsSerialized: "+myParamsSerialized);
//
//				MyParams newParams = null;
//				
//				 // deserialize the object
//				 try {
//				     byte b[] = myParamsSerialized.getBytes(); 
//				     ByteArrayInputStream bi = new ByteArrayInputStream(b);
//				     ObjectInputStream si = new ObjectInputStream(bi);
//				     newParams = (MyParams) si.readObject();
//				 } catch (Exception e) {
//				     System.out.println(e);
//				 }
//				 
//					System.out.println("newParams.getDay(): "+newParams.getDay().toString());
//					System.out.println("newParams.getMonth: "+newParams.getMonth().toString());
//				
//				List<Cdr> allCdrs = cdrService.searchCdrs();
//				System.out.println("Nr of CDRs: "+allCdrs.size());
//				for (Cdr x : allCdrs) {
//					x.getEnums().size();
//					result = result + "Day("+ x.getDay()+") ";
//					Day dayx = x.getDay();
//					x.getPhoneNumbers().size();
//					List<String> phonenumbers = x.getPhoneNumbers();
//					if (phonenumbers != null){
//						for (String pn : phonenumbers) {
//
//							result = result + "Pn("+pn+") ";
//						}
//					}
//					List<Enum> props = x.getEnums();
//					if (props != null){
//						for (Enum pr : props) {
//							result = result + "pr("+pr.toString()+") ";
//						}
//					}
//					System.out.println("dayx("+dayx+")");
//				}
				return result;
			}
			if (msg.equals("cmd:deletecdr")){ 
				trace(5,"serviceCallBack - cmd:deletecdr");
//				cdrService.deleteCdr(new Integer(620));
				result = "deleted...";
				return result;
			}
			if (msg.indexOf("cmd:dump")>=0){
				trace(5,"serviceCallBack - dump...");
				List<AppConfig> apps = appService.searchApps();
				int size = apps.size();
				for (int i = 0;i<size;i++){
					result = result+apps.get(i).toString()+ '\r' + '\n';
				}
				return result;
			}
			if (msg.indexOf("cmd:writevars")>=0){  
				trace(5,"serviceCallBack - write Variables");	
				configService.writeConfigFile(afw.getSystemVariables(), "/opt/svp/sample/config/sampletest.properties");
			}
			if (msg.indexOf("cmd:setversion")>=0){  
				trace(5,"serviceCallBack - test set version...");	
				afw.version = afw.version+") \n"+"Test set Version...";
			}
			if (msg.indexOf("cmd:cc")>=0){ 
				trace(5,"serviceCallBack - CreateCall");
				CallParams cp = new CallParams();
				cp.setCli("OutCallTest");
				cp.setNumber("OutCallTest");
				this.createCall(cp);
			}
			if (msg.equals("detach")){
				trace(5,"serviceCallBack - detach");	
				detachFramework();
				detachedManually = true;
			}
			if (msg.equals("attach")){
				trace(5,"serviceCallBack - attach");	
				attachFramework();
				detachedManually = false;
			}
			if (msg.equals("purgefiles")){
				trace(5,"serviceCallBack - Purge files");	
				purgeFiles();
			}
			if (msg.equals("readcfg")){
				trace(5,"serviceCallBack - readcfg");
				readConfig();
			}
			if (msg.indexOf("cmd:sa")>=0){   // Used to start an application from appctl interface
				trace(5,"serviceCallBack - Start Application");	
				int i=1;
				if (msg.length()>6){
				    i = Tools.stringToInt(msg.substring(6));
				}
				for (int j=0;j<i;j++){
					afw.createCall("DbTest");
				}
			}
			return("ApplicationFrameworkService.serviceCallBack");
		}catch (Exception e){
			trace(2,"serviceCallBack - Exception: "+e);
			return "";
		} 
	}
	


	/**
	 * Trace routine for ApplicationFrameworkService
	 * logs will be written to FrameworkService.log
	 */
	public void trace(int level, String msg){
//       switch(level){ 
//        case 0: 
//    		logger.error(msg);
//            break; 
//        case 1: 
//    		logger.error(msg);
//            break; 
//        case 2: 
//    		logger.warn(msg);
//            break; 
//        case 3: 
//    		logger.info(msg);
//            break; 
//        default: 
//    		logger.debug(msg);
//        } 			
		if (afw==null){
			System.out.println("ApplicationFrameworkService."+msg);
		}else{
		    if (afw.debugLevel<level){
		        return;
		    }
			System.out.println("ApplicationFrameworkService."+msg);
			afw.trace(level,"ApplicationFrameworkService."+msg);
		}
	}
    
	/**
	 * Method returns system-persistent Data object
	 */
	public SysPersData getSpd() {
		return spd;
	}

	/**
	 * initForExternalServices method
	 */
	public void initForExternalServices(){
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// 
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		try{
			// do here appl. spec. init
			readConfig();
			// Example to work with System Variables
			this.setSystemVariable("Service","Sample");
			trace(5,"SystemVariable Service:"+getSystemVariable("Service"));
		}catch (Exception e){
			trace(2,"init - Exception: "+e);
		} 
	}

	/**
	 * readConfig method
	 */
	public void readConfig(){
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// 
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		try{
		    if (this.spd==null){
		    	System.out.println("DefaultFrameworkService.read config - run Method not executed till now - init first...");
		    	init();
		    }
			String configData = afw.xmldata;
			trace(5,"configData:"+configData);
			// read service specifig config...
		}catch (Exception e){
			trace(2,"init - Exception: "+e);
		} 
	}


	/**
	 * This method will be called, to check periodically the Status of external services 
	 */
	public void checkStatus(){
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// 
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		try{
			trace(5,"checkStatus - start");
			// ***********************
/**			for (int i=0;i<100;i++){
				   int numberOfMillisecondsInTheFuture = i*1000; // 10 sec
				    Date timeToRun = new Date(System.currentTimeMillis()+numberOfMillisecondsInTheFuture);
				    Timer timer = new Timer();
				    
				    timer.schedule(new TimerTask() {
			            public void run() {
			                System.out.println("This is the timer task ****************************");
			                checkTest();
			            }
				    }, timeToRun);
			}
			*/
			// ***********************
			this.timeCtr++;
			if (timeCtr >= 30){   // all 15 Minutes
				timeCtr=0;
				trace(5,"checkStatus - 15min Tests - nothing implemented yet");
			}
		}catch (Exception e){
			e.printStackTrace(System.out);
			trace(2,"checkStatus - Exception: "+e);
		} 
	}

	
	public void purgeFiles(){
		try{
			trace(5,"purgeFiles - start");
			// implement Purging of files, if necessary
		}catch (Exception e) {
			trace(2,"purgeFiles - Exception: "+e);
		}
	}
	
	
	public int getNrApplInstances(){
		try{
			BitSet applBitSet = afw.getApplBitSet();
			String applBits = applBitSet.toString();
			int size = applBitSet.size();
			int used = 0;
			for (int i=0;i<size;i++){
				if (applBitSet.get(i)){
					used++;
				}
			}
			trace(6,"getNrApplInstances("+applBits+")");
			trace(6,"getNrApplInstances used("+used+")");
			return used;
		}catch (Exception e) {
			trace(2,"getNrApplInstances - Exception: "+e);
			return -1;
		}
	}

	/**
	 * This method will be called, when System has been stopped 
	 * with appctl "stop"
	 * cleanup connections!
	 */
	public void shutdown(){
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// 
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		try{
			trace(5, "shutdown - begin...");
			if (shutdownCalled){
				trace(5, "shutdown - shutdown already called - return...");			
				return;
			}
			shutdownCalled = true;
		}catch (Exception e){
			trace(2,"shutdown - Exception: "+e);
		} 
	}


    /**
     * Used to get Variable String-Values
     */
    public String getSystemVariable(String name){
    	try{          
    		/**
    		 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    		 *
    		 *
    		 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    		 */
    		return afw.getSystemVariable(name);
    	} catch (Exception ex) {
    		trace(2,"getSystemVariable - Exception: "+ex);
    		ex.printStackTrace(System.out);
    		return "";
    	}
    }
     
    /**
     * Used to set Variable String-Values
     */
    public void setSystemVariable(String name, String value){
    	try{          
    		/**
    		 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    		 *
    		 *
    		 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    		 */
    		afw.setSystemVariable(name, value);
    	} catch (Exception ex) {
    		trace(2,"setSystemVariable - Exception: "+ex);
    		ex.printStackTrace(System.out);
    	}
    } 

    
	/**
	 * Trace routine for external Services, like DB-Interface, etc. ....
	 * logs will be written to given filename
	 */
	public void traceExtService(String filename, int level, String msg){
		if (afw==null){
			System.out.println(filename+"."+msg);
		}else{
		    if (afw.debugLevel<level){
		        return;
		    }
			System.out.println(filename+"."+msg);
			afw.traceExtService(filename,level,filename+"."+msg);
		}
	}
    
	/**
	 * Trace routine for ApplicationFramework
	 * logs will be written to Framework.log
	 * Wiil be used to send SNMP-Traps!!!
	 */
	public void traceFramework(int level, String msg){
		if (afw==null){
			System.out.println("Framework."+msg);
		}else{
		    if (afw.debugLevel<level){
		        return;
		    }
			System.out.println("Framework."+msg);
			afw.fwt.trace(level,"ApplicationFrameworkService."+msg);
		}
	}

	/**
	 * Trace routine for ApplicationFramework
	 * logs will be written to Framework.log
	 * Wiil be used to send SNMP-Traps!!!
	 */
	public synchronized void writeStatisticData(String data){
		if (afw==null){
			System.out.println("writeStatisticData - (afw==null)!!!!!!!!!!!!!");
		}else{
			afw.fwt.writeStatisticData(data);
		}
	}
	
	public String getFrameworkInfo() {
		try {
			trace(5, "getFrameworkInfo - begin...");
			String result = "Sonorys Application Framework Version "+at.sonorys.svp.framework.Version.version+" ("+at.sonorys.svp.framework.Version.time+") ("+afw.version+")" + "<br>";
			result = result + "Start of Framework: " + afw.startOfFramework + "<br>";
			result = result + "Application: " + afw.applicationName + "<br>";
			result = result + "***************** CONFIG DATA ******************************" + "<br>";
			result = result + "ConfigFile: " + afw.configFile + "<br>";
			result = result + "ApplicationData: " + afw.appConfigFile + "<br>";
			result = result + "LogDirectory: " + afw.logDir + "<br>";
			result = result + "ActualLogDirectory: " + afw.completeLogDir + "<br>";
			result = result + "ORBInitialHost: " + afw.orbInitialHost + "<br>";
			result = result + "ORBInitialPort: " + afw.orbInitialPort + "<br>";
			result = result + "MaxApplications: " + afw.maxApplications + "<br>";
			result = result + "ContextName: " + afw.contextName + "<br>";
			result = result + "ServiceNumber: " + afw.serviceNumber + "<br>";
			result = result + "MpCcManager: " + afw.mpCcManager + "<br>";
			result = result + "UiManager: " + afw.uiManager + "<br>";
			result = result + "UiManager: " + afw.uiManager + "<br>";
			result = result + "UiManager: " + afw.uiManager + "<br>";
			result = result + "UiManager: " + afw.uiManager + "<br>";
			result = result + "DebugLevel: " + afw.debugLevel + "<br>";
			if (afw.purgeLogFilesInDays > 0) {
				result = result + "PurgeLogFilesInDays: " + afw.purgeLogFilesInDays + "<br>";
			} else {
				result = result + "PurgeLogFilesInMonths: " + afw.purgeLogFilesInMonths + "<br>";
			}
			result = result + "PurgingHour: " + afw.purgingHour + "<br>";
			result = result + "MachineName: " + afw.machineName + "<br>";
			result = result + "CommPort: " + afw.commPort + "<br>";
			result = result + "TracingPorts: " + afw.monitoringPort + "<br>";
			result = result + "CheckStatusTO: " + afw.checkStatusTO + "<br>";
			result = result + "Sum of all calls since Start of Framework: " + String.valueOf(afw.totalCallCounter)
					+ "<br>";
			return result;
		} catch (Exception ex) {
			trace(2, "getFrameworkInfo - Exception: " + ex);
			ex.printStackTrace(System.out);
			return ex.toString();
		}
	}
	
	public String getActiveApplicationInstances() {
		try {
			trace(5, "getActiveApplicationInstances - begin...");
			String result = "Sonorys Application Framework Version "+at.sonorys.svp.framework.Version.version+" ("+at.sonorys.svp.framework.Version.time+") ("+afw.version+")" + "<br>";
			result = result + "Actual Applications: " + afw.getApplArray() + "<br>";
			return result;
		} catch (Exception ex) {
			trace(2, "getActiveApplicationInstances - Exception: " + ex);
			ex.printStackTrace(System.out);
			return ex.toString();
		}
	}
	
	public String getFrameworkStatus() {
		try {
			trace(5, "getFrameworkStatus - begin...");
			String result = "Sonorys Application Framework Version " + at.sonorys.svp.framework.Version.version + " ("
					+ at.sonorys.svp.framework.Version.time + ") (" + afw.version + ")" + "<br>";
			result = result + "<br>";
			if (afw.simMode) {
				result = result + "ApplicationFramework running without OSA/CORBA Interface" + "<br>";
			} else {
				if (Tools.isValueSet(afw.getSystemVariable("DisableManagerInterruptedInfo"))) {
					trace(3, "getFrameworkStatus - do not send ManagerInterruptedInfo...");
				} else {
					result = result + "ManagerInterrupted: " + afw.managerInterrupted + "<br>";
					if (afw.managerInterrupted) {
						result = result + "###########################################################" + "<br>";
						result = result + "#####                                                 #####" + "<br>";
						result = result + "#####     ManagerInterrupted has been received!!!     #####" + "<br>";
						result = result + "#####                                                 #####" + "<br>";
						result = result + "###########################################################" + "<br>";
					}
				}
				result = result + "CORBA Naming Service Available: " + afw.corbaNamingServiceAvailable + "<br>";
				result = result + "CORBA SVP Context Available: " + afw.svpCorbaContextAvailable + "<br>";
				result = result + "CORBA MultiPartyCallControlManager Reference Available: "
						+ afw.mpccmReferenceAvailable + "<br>";
				result = result + "CORBA UserInterfaceManager Reference Available: " + afw.uimReferenceAvailable
						+ "<br>";
				result = result + "Application Attached: " + afw.getApplicationAttached() + "<br>";
			}
			result = result + "<br>";
			result = result + "Sample Application O.K....(" + Tools.getTime() + ")" + "<br>";
			return result;
		} catch (Exception ex) {
			trace(2, "getFrameworkStatus - Exception: " + ex);
			ex.printStackTrace(System.out);
			return ex.toString();
		}
	}
	
	public String purgeFrameworkLogs() {
		try {
			trace(5, "purgeFrameworkLogs - begin...");
			afw.fwt.purgeStatDirectory();
			afw.fwt.purgeLogDirectory();
			String result = "Sonorys Application Framework Version "+at.sonorys.svp.framework.Version.version+" ("+at.sonorys.svp.framework.Version.time+") ("+afw.version+")" + "<br>";
			result = result + "purge finished...("+Tools.getTime()+")" + "<br>";
			return result;
		} catch (Exception ex) {
			trace(2, "purgeFrameworkLogs - Exception: " + ex);
			ex.printStackTrace(System.out);
			return ex.toString();
		}
	}
	
	public String readFrameworkConfig() {
		try {
			trace(3, "readFrameworkConfig - begin...");
			String result = "Sonorys Application Framework Version "+at.sonorys.svp.framework.Version.version+" ("+at.sonorys.svp.framework.Version.time+") ("+afw.version+")" + "<br>";
			result = result + "read FrameworkConfig - begin....("+Tools.getTime()+")" + "<br>";
			FrameworkConfiguration frameworkConfiguration = frameworkConfig.prepareConfig();
			if (frameworkConfiguration!=null){
				trace(3, "readFrameworkConfig - store new frameworkConfiguration to ApplicationFramework...");
				afw.setFrameworkConfiguration(frameworkConfiguration);
			}
			boolean confFileValid =  afw.fwt.readXMLfile(false);
			readConfig();
			if (confFileValid==false){
				result = "********************************************************************************************"+ "<br>";
				result = result + "***** CONFIGFILES NOT VALID !!! "+ "<br>";
				result = result + "***** For Details see: Tracing.log"+ "<br>";
				result = result + "********************************************************************************************"+ "<br>";          	
	        }else{
	        	result = result + "read configfile finished...("+Tools.getTime()+")" + "<br>";
	        }     
			return result;
		} catch (Exception ex) {
			trace(2, "readFrameworkConfig - Exception: " + ex);
			ex.printStackTrace(System.out);
			return ex.toString();
		}
	}
	
	public String readFrameworkConfigAndCallflow() {
		try {
			trace(5, "readFrameworkConfigAndCallflow - begin...");
			String result = "Sonorys Application Framework Version "+at.sonorys.svp.framework.Version.version+" ("+at.sonorys.svp.framework.Version.time+") ("+afw.version+")" + "<br>";
			result = result + "read FrameworkConfig - begin....("+Tools.getTime()+")" + "<br>";
			FrameworkConfiguration frameworkConfiguration = frameworkConfig.prepareConfig();
			if (frameworkConfiguration!=null){
				trace(3, "readFrameworkConfigAndCallflow - store new frameworkConfiguration to ApplicationFramework...");
				afw.setFrameworkConfiguration(frameworkConfiguration);
			}
			boolean confFileValid =  afw.fwt.readXMLfile(true);
			readConfig();
			if (confFileValid==false){
				result = "********************************************************************************************"+ "<br>";
				result = result + "***** CONFIGFILES NOT VALID !!! "+ "<br>";
				result = result + "***** For Details see: Tracing.log"+ "<br>";
				result = result + "********************************************************************************************"+ "<br>";          	
	        }else{
	        	result = result + "read configfile finished...("+Tools.getTime()+")" + "<br>";
	        }     
			return result;
		} catch (Exception ex) {
			trace(2, "readFrameworkConfigAndCallflow - Exception: " + ex);
			ex.printStackTrace(System.out);
			return ex.toString();
		}
	}
	
	public void testCdrInterface() {
		try {
			trace(5, "testCdrInterface - begin...");
			Cdr cdr = new Cdr();
			cdr.setMsisdn("0043");
			cdr.setMisc("VMS test CDR Service...("+Tools.getTime()+")");
//			cdrService.persistCdr(cdr);
		} catch (Exception ex) {
			trace(2, "testCdrInterface - Exception: " + ex);
			ex.printStackTrace(System.out);
		}
	}

	public String controllerShutdown(int returnValue) {
		try {
			trace(5, "controllerShutdown - begin...");
			this.returnValue = returnValue;
			this.shutdownEnabled = true;
			frameworkController.shutdownFramework();
			return "controllerShutdown - finished...";
		} catch (Exception ex) {
			trace(2, "controllerShutdown - Exception: " + ex);
			ex.printStackTrace(System.out);
			return "controllerShutdown - Exception: " + ex.toString();
		}
	}

	public int getReturnValue() {
		return returnValue;
	}

	public void setReturnValue(int returnValue) {
		this.returnValue = returnValue;
	}

	public boolean isShutdownEnabled() {
		return shutdownEnabled;
	}

	public void setShutdownEnabled(boolean shutdownEnabled) {
		this.shutdownEnabled = shutdownEnabled;
	}

	public FrameworkConfiguration readFrameworkConfiguration() {
		try {
			trace(3, "readFrameworkConfiguration - begin...");
			Hashtable<String, String> configVariables = configService.readConfigFile(frameworkConfig.getConfigFile());
			AppConfig appConfig = appService.getAppConfig(frameworkConfig.getApplicationName());	
			FrameworkConfiguration frameworkConfiguration = appConfig.getFrameworkConfiguration();
			frameworkConfiguration.setVariables(configVariables);
			return frameworkConfiguration;
		} catch (Exception ex) {
			trace(2, "readFrameworkConfiguration - Exception: " + ex);
			ex.printStackTrace(System.out);
			return null;
		}
	}

}
