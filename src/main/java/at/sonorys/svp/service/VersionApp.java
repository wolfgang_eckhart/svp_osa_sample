/*
 * Version.java
 * 
 * Created on 23.09.2004
 *
 * Sonorys Technology GesmbH
 * Always ready for Conversation
 * 
 */
package at.sonorys.svp.service;
 
/**
 * @author eckhart
 *
 */


/**
 * Class holds Version-string
 */
	
public class VersionApp {

	public static String version = "<br>" + "Sample Application Version 5.0.6 (2016-11-16 16:18:00) (Build Nr.: 2502)";
}
