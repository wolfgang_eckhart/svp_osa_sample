package at.sonorys.svp.service.impl;

import at.sonorys.svp.entity.Cdr;
import at.sonorys.svp.repository.CdrRepository;
import at.sonorys.svp.service.api.CdrApi;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CdrService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CdrRepository cdrRepository;

//	@Transactional
//	@Override
//	public Integer persistCdr(Cdr cdr) {
//		cdr = cdrRepository.saveAndFlush(cdr);
//
//		logger.debug("Cdr persisted with id [{}]",
//				cdr.getId());
//
//		return cdr.getId();
//	}
//
//	@Transactional(readOnly = true)
//	@Override
//	public List<Cdr> searchCdrs() {
//		return cdrRepository.findAll();
//	}

//	@Override
//	public Boolean deleteCdr(Integer id) {
//		cdrRepository.delete(id);
//		return true;
//	}

}
