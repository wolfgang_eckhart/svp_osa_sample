package at.sonorys.svp.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import at.sonorys.svp.entity.AppConfig;
import at.sonorys.svp.entity.AppVariable;
import at.sonorys.svp.repository.AppVariableRepository;
import at.sonorys.svp.service.api.AppVariableApi;

@Service
public class AppVariableService implements AppVariableApi {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AppVariableRepository appVariableRepository;
	@Transactional
	@Override
	public Integer persistAppVariable(AppVariable appVariable) {
		appVariable = appVariableRepository.saveAndFlush(appVariable);
		
		logger.debug("AppVariable persisted with name [{}]",
				appVariable.getName());
		
		return appVariable.getId();
	}

	@Transactional(readOnly = true)
	@Override
	public List<AppVariable> searchAppVariables() {
		return appVariableRepository.findAll();
	}

	@Override
	public AppVariable getAppVariable(String name) {
		List<AppVariable> appVariables = appVariableRepository.findByName(name);
		if (appVariables.size()>0){
			logger.info("Found with name("+name+") appVariable("+ appVariables.get(0).toString()+")");
			return appVariables.get(0);
		}else{
			logger.info("No appVariable found with name("+name+")");
			return null;
		}		
	}

}
