package at.sonorys.svp.service.impl;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import at.sonorys.svp.service.api.ConfigApi;

@Service
public class ConfigService implements ConfigApi {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public Hashtable<String, String> readConfigFile(String configFile) {
		Hashtable<String, String> variables = new Hashtable<String, String>();
		try {
			System.out.println("readConfigFile with filename("+configFile+")");
			logger.debug("readConfigFile with filename [{}]",
					configFile);
			Properties props = new Properties();
			InputStream input = null;
			input = new FileInputStream(configFile);
			props.load(input);
			Enumeration<?> entries = props.keys();
			while(entries.hasMoreElements()){
				String param = (String) entries.nextElement();
				String value = props.getProperty(param);
				System.out.println("Param: "+param+" ["+value+"]");
				variables.put(param,value);
			}
			return variables;
		} catch (IOException e) {
			System.out.println("readConfigFile - Exception: "+e);
			e.printStackTrace();
		}		
		return null;
	}

	@Override
	public int writeConfigFile(Hashtable<String, String> variables, String filename) {
		OutputStream output = null;
		try {
			Enumeration<String> entries = variables.keys();
			Properties props = new Properties();
			while (entries.hasMoreElements()) {
				String param = (String) entries.nextElement();
				String value = variables.get(param);
				System.out.println("Param: " + param + " [" + value + "]");
				props.setProperty(param, value);
			}
			output = new FileOutputStream(filename);
			props.store(output, null);
			return 0;
		} catch (Exception e) {
			System.out.println("writeConfigFile - Exception: " + e);
			e.printStackTrace();
			return -1;
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
