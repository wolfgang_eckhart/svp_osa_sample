package at.sonorys.svp.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import at.sonorys.svp.entity.AppConfig;
import at.sonorys.svp.repository.AppConfigRepository;
import at.sonorys.svp.service.api.AppConfigApi;

@Service
public class AppConfigService implements AppConfigApi {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AppConfigRepository appRepository;

	@Transactional
	@Override
	public Integer persistApp(AppConfig app) {
		app = appRepository.saveAndFlush(app);
		
		logger.debug("App persisted with name [{}]",
				app.getApplicationName());
		
		return app.getId();
	}

	@Transactional(readOnly = true)
	@Override
	public List<AppConfig> searchApps() {
		return appRepository.findAll();
	}

	@Override
	public AppConfig getAppConfig(String applicationName) {
		List<AppConfig> appConfigs = appRepository.findByApplicationName(applicationName);
		if (appConfigs.size()>0){
			logger.info("Found with applicationName("+applicationName+") appConfig("+ appConfigs.get(0).toString()+")");
			return appConfigs.get(0);
		}else{
			logger.info("No appConfig found with applicationName("+applicationName+")");
			return null;
		}		
	}

}
