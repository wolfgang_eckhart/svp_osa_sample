package at.sonorys.svp.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import at.sonorys.svp.service.FrameworkServiceOperations;
import at.sonorys.svp.service.api.CommandsApi;

@Service
public class CommandsService implements CommandsApi {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private FrameworkServiceOperations frameworkServiceOperations;

	@Override
	public String getStatus() {
		System.out.println("CommandsService.getStatus...");
		return frameworkServiceOperations.serviceCallBack("Status");
	}


}
