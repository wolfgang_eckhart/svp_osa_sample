package at.sonorys.svp.service.test;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Hashtable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import at.sonorys.svp.service.impl.ConfigService;

public class ConfigTest {

	@Autowired
	private ConfigService configService;

//	String configFile = "/opt/svp/vms/config/vms1.properties";
//	String testFile = "/opt/svp/vms/config/vmstest1.properties";
//
//	@Before
//	public void setUp() {
//		System.out.println("create ConfigService...");
//		configService = new ConfigService();
//	}
//
//	@Test
//	public void writeValidFile() {
//		Hashtable<String, String> vars = new Hashtable<String, String>();
//		vars.put("key1", "value1");
//		vars.put("key2", "value2");
//		int result = configService.writeConfigFile(vars, testFile);
//		assertTrue("Write config file should return 0, if no Exception...", result == 0);
//	}
//
//	@Test
//	public void writeInvalidFile() {
//
//		int result = configService.writeConfigFile(null, testFile);
//		System.out.println("result("+result+")");
//		assertTrue("Write config file should return -1, if Hashtable was null...", result == -1);
//	}
//
//	@Test
//	public void readTestFile() {
//		Hashtable<String, String> vars = configService.readConfigFile(configFile);
//		assertTrue("Write config file should return 0, if no Exception...", vars.size() > 1);
//	}
//
//	@Test
//	public void readInvalidFile() {
//		Hashtable<String, String> vars = configService.readConfigFile("/opt/xxx.xxx");
//		assertTrue("Write config file should return 0, if no Exception...", vars==null);
//	}
//
//	@After
//	public void cleanUp() {
//		System.out.println("remove Testfile...");
//		File file = new File(testFile);
//		file.delete();
//	}
	@Test
	public void dummyTest() {
		assertTrue("Dummy Test...", 1 > 0);
	}

}
