package at.sonorys.svp.respository.cdrrepository;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import at.sonorys.svp.AbstractTransactionServiceTest;
import at.sonorys.svp.entity.Cdr;
import at.sonorys.svp.repository.CdrRepository;
import at.sonorys.svp.testtools.testentityfactories.CdrTestEntityFactory;

public class FindByMiscTest extends AbstractTransactionServiceTest {
	
	@Autowired
	private CdrTestEntityFactory cdrTestEntityFactory;

	@Autowired
	private CdrRepository cdrRepository;
	
	private Cdr cdr;
	
	@Before
	public void setUp() {
		cdr = cdrTestEntityFactory.getCdr();
		cdr = cdrRepository.saveAndFlush(cdr);
		System.out.println(cdr.getId());
	}
	
	@Test
	public void withInvalidMisc_shouldReturnEmptyList() {
		String notExistingMisc = "someNotExistingMisc";
		
		List<Cdr> cdrs = cdrRepository.findByMisc(notExistingMisc);
		
		assertTrue("No Cdr should be found since no one with the given misc is persisted",
				cdrs.isEmpty());
	}
	
	@Test
	public void shouldFindCdr() {
		List<Cdr> cdrs = cdrRepository.findByMisc(cdr.getMisc());
		
		assertEquals("Result should contain 1 cdr since only one is persisted with the given misc",
				1,
				cdrs.size());
		assertEquals("Cdr should be in result since it contains the search misc",
				cdr,
				cdrs.get(0));
	}
	
}
