package at.sonorys.svp.respository.appconfigrepository;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import at.sonorys.svp.AbstractTransactionServiceTest;
import at.sonorys.svp.entity.Cdr;
import at.sonorys.svp.entity.AppConfig;
import at.sonorys.svp.repository.CdrRepository;
import at.sonorys.svp.repository.AppConfigRepository;
import at.sonorys.svp.testtools.testentityfactories.CdrTestEntityFactory;
import at.sonorys.svp.testtools.testentityfactories.AppConfigTestEntityFactory;

public class CreateAppConfigTest extends AbstractTransactionServiceTest {
	
	@Autowired
	private AppConfigTestEntityFactory appTestEntityFactory;

	@Autowired
	private AppConfigRepository appRepository;
	
	private AppConfig appConfig;
	private String applicationName = "SampleWE";
	
	
	@Test
	public void createAppConfig() {
		
		appConfig = appTestEntityFactory.getDefaultConfig(applicationName);
		appConfig = appRepository.saveAndFlush(appConfig);
		List<AppConfig> appConfigs = appRepository.findByApplicationName(applicationName);
		String result = "";
		int size = appConfigs.size();
		for (int i = 0;i<size;i++){
			result = result+appConfigs.get(i).toString()+ '\r' + '\n';
		}
		System.out.println("getSVPApplication - created: "+result);
		assertTrue("AppConfig should be persisted...",
				appConfigs.size()>0);
	}
	

	
}
