package at.sonorys.svp.respository.appconfigrepository;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import at.sonorys.svp.AbstractTransactionServiceTest;
import at.sonorys.svp.entity.Cdr;
import at.sonorys.svp.entity.AppConfig;
import at.sonorys.svp.repository.CdrRepository;
import at.sonorys.svp.service.impl.AppConfigService;
import at.sonorys.svp.repository.AppConfigRepository;
import at.sonorys.svp.testtools.testentityfactories.CdrTestEntityFactory;
import at.sonorys.svp.testtools.testentityfactories.AppConfigTestEntityFactory;

public class FindAppConfigTest extends AbstractTransactionServiceTest {
	
	@Autowired
	private AppConfigTestEntityFactory appTestEntityFactory;

	@Autowired
	private AppConfigRepository appRepository;
	
	@Autowired
	private AppConfigService appConfigService;
	
	private AppConfig svpApplication;
	
	@Before
	public void setUp() {
		svpApplication = appTestEntityFactory.getApp();
		svpApplication = appRepository.saveAndFlush(svpApplication);
		
		System.out.println("setUp - svpApplication: "+svpApplication.getApplicationName());
	}
	
	@Test
	public void getSVPApplication() {
		
		List<AppConfig> apps = appRepository.findByApplicationName("Sample500");
		String result = "";
		int size = apps.size();
		for (int i = 0;i<size;i++){
			result = result+apps.get(i).toString()+ '\r' + '\n';
		}
		System.out.println("getSVPApplication - found: "+result);
		assertTrue("Sample1 SVPApplication should be found, because is has been persited at setUp...",
				apps.size()>0);
	}
	
	@Test
	public void getAppConfig() {
		
		AppConfig appConfig = appConfigService.getAppConfig("Sample500");
		if (appConfig!=null){
			System.out.println("getSVPApplication - found: "+appConfig.toString());			
		}
		assertTrue("template SVPApplication should be found, because it should always in the table...",
				appConfig!=null);
	}
	
//	@Test
//	public void shouldFindCdr() {
//		List<Cdr> cdrs = cdrRepository.findByMisc(cdr.getMisc());
//		
//		assertEquals("Result should contain 1 cdr since only one is persisted with the given misc",
//				1,
//				cdrs.size());
//		assertEquals("Cdr should be in result since it contains the search misc",
//				cdr,
//				cdrs.get(0));
//	}

	@Test
	public void dummyTest() {
		assertTrue("Dummy Test...", 1 > 0);
	}

	
}
