package at.sonorys.svp.testtools.testentityfactories;

import org.springframework.stereotype.Service;

import at.sonorys.svp.entity.AppConfig;

@Service
public class AppConfigTestEntityFactory {
	
	public AppConfig getApp() {
		AppConfig appConfig = new AppConfig();
		
		appConfig.setApplicationName("Sample500");
		appConfig.setOrbInitialHost("svp651");
		
		return appConfig;
	}

	public AppConfig getDefaultConfig(String applicationName) {
		AppConfig appConfig = new AppConfig();
		
		appConfig.setApplicationName(applicationName);
		appConfig.setApplicationRootDirectory("/opt/svp/"+applicationName+"/");
		appConfig.setOrbInitialHost("svp651");
		appConfig.setOrbInitialPort(2809);
		appConfig.setContextName("SVP");
		appConfig.setMpCcManager("MPCCM");
		appConfig.setUiManager("UIM");
		appConfig.setServiceNumber("1500*");
		appConfig.setDebugLevel("5");
		appConfig.setMaxApplications(480);
		appConfig.setLogDir("/var/opt/svp/"+applicationName+"/log1");
		appConfig.setMaxLogfileSize(10);
		appConfig.setStatDir("/var/opt/svp/statistics");
		appConfig.setStatFilename(applicationName);
		appConfig.setPurgeLogfilesInDays(10);
		appConfig.setPurgeStatfilesInDays(30);
		appConfig.setPurgingHour(1);
		appConfig.setCheckStatusTo(30);
		appConfig.setCommPort(24999);
		appConfig.setValidateConfFiles(true);
		appConfig.setSnmpHost("dalton");
		appConfig.setSnmpPort(161);
		appConfig.setAssembleCallflowFile(true);
		appConfig.setSaveOldCallflowFile(true);
		appConfig.setCallflowFileList("/opt/svp/"+applicationName+"/config/callflowFileList.xml");
		appConfig.setMachineName("172.24.2.36");
		appConfig.setApplicationData("/opt/svp/"+applicationName+"/config/sample.xml");
	
		return appConfig;
	}

//	private String applicationName;
//	private String orbInitialHost;
//	private Integer orbInitialPort;
//	private String contextName;
//	private String mpCcManager;
//	private String uiManager;
//	private String serviceNumber;
//	private String debugLevel;
//	private String maxApplications;
//	private String logDir;
//	private Integer maxLogfileSize;
//	private String statDir;
//	private String statFilename;
//	private Integer purgeLogfilesInDays;
//	private Integer purgeStatfilesInDays;
//	private Integer purgingHour;
//	private Integer checkStatusTo;
//	private Integer commPort;
//	private Boolean validateConfFiles;
//	private String snmpHost;
//	private Integer snmpPort;
//	private String assembleCallflowFile;
//	private String saveOldCallflowFile;
//	private String callflowFileList;
//	private String machineName;
//	private String applicationData;

	//<ApplicationName>sample_1</ApplicationName>	
	//<ORBInitialHost>svp651</ORBInitialHost>	<!-- NameServer host name -->
	//<ORBInitialPort>2809</ORBInitialPort>
	//<ContextName>SVP</ContextName>
	//<MpCcManager>MPCCM</MpCcManager>
	//<UiManager>UIM</UiManager> 
	//<ServiceNumber>1500*</ServiceNumber>
	//<DebugLevel>5</DebugLevel>
	//<MaxApplications>480</MaxApplications>
	//<LogDir>/var/opt/svp/sample/log1</LogDir>
	//<MaxLogFileSize>10</MaxLogFileSize> <!-- in MB -->
	//<StatDir>/var/opt/svp/statistics</StatDir>
	//<StatFileName>sample</StatFileName>
	//<PurgeLogFilesInDays>10</PurgeLogFilesInDays>
	//<PurgeStatFilesInDays>30</PurgeStatFilesInDays>
	//<PurgingHour>1</PurgingHour>
	//<CheckStatusTO>10</CheckStatusTO>
	//<CommPort>24900</CommPort>
	//<ValidateConfFiles>Yes</ValidateConfFiles>


}
