package at.sonorys.svp.testtools.testentityfactories;

import org.springframework.stereotype.Service;

import at.sonorys.svp.entity.Cdr;

@Service
public class CdrTestEntityFactory {
	
	public Cdr getCdr() {
		Cdr cdr = new Cdr();
		
		cdr.setMisc("misc1");
		cdr.setMsisdn("01234");
		
		return cdr;
	}

}
