161117:
=======
http://www.baeldung.com/spring-data-jpa-multiple-databases

161110:
=======
- make new Coding with all lowercase
	- "Table" appconfig
	- all fields in lowercase
				- id (ApplicationInstance)
				- name "Sample"
				- orb_initial_host
				- orb_initial_port
				- context_name
				- mp_cc_manager
				- ui_manager
				- service_number
				- debug_level
				- application_data "/opt/svp/sample/config/sample.xml"


http://docs.spring.io/spring/docs/current/spring-framework-reference/html/orm.html

161110:
=======
- if changing TableName in entity definition Call following Problem occures on JUnitTest:
	@Entity
	@Table(name="Applications")
	public class App {

	Caused by: org.postgresql.util.PSQLException: ERROR: could not create file "base/16394/17926": No space left on device

- when changing back (with no TableDefinition Annotation), it's working again... ("No space left... error" will not occure anymore...) 

161110:
=======
- install new database on bod681

"su - postgres"
/usr/pgsql-9.2/bin/initdb -D /var/lib/pgsql/9.2/svp_db/data  --no-locale --encoding='UTF8'
.
.
.
Success. You can now start the database server using:

    /usr/pgsql-9.2/bin/postgres -D /var/lib/pgsql/9.2/svp_db/data
or
    /usr/pgsql-9.2/bin/pg_ctl -D /var/lib/pgsql/9.2/svp_db/data -l logfile start

bash-4.1$

- edit pg_hba.conf

- edit postgresql.conf

     listen_addresses = '*'
     port = 6444
     max_connections = 20 
     log_directory = '/var/opt/svpdb/pglog'
     log_filename = 'postgresql-%Y-%m-%d.log'      # log file name pattern,
     log_rotation_size = 50MB                        # Automatic rotation of logfiles will

- create logdir

[root@bod681 9.2]# cd /var/opt/
[root@bod681 opt]# mkdir svpdb
[root@bod681 opt]# chmod 777 svpdb
[root@bod681 opt]# cd svpdb/
[root@bod681 svpdb]# mkdir pglog
[root@bod681 svpdb]# chmod 777 pglog/
[root@bod681 svpdb]#


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Apparently you did "su postgres" from the root account, so you're
still in root's home directory.  It'd be better to do "su - postgres"
to ensure you've acquired all of the postgres account's environment.
Reading "man su" might help you out here.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

"su - postgres"  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	[root@bod681 ~]# su - postgres

- start db
	postgres user("su - postgres")!!!!!!!!!!!!!!!!!!!
	  /usr/pgsql-9.2/bin/pg_ctl -D /var/lib/pgsql/9.2/svp_db/data start
	
	-bash-4.1$ /usr/pgsql-9.2/bin/pg_ctl -D /var/lib/pgsql/9.2/svp_db/data start
	server starting
	-bash-4.1$

- stop db
	postgres user("su - postgres")!!!!!!!!!!!!!!!!!!!
	  /usr/pgsql-9.2/bin/pg_ctl -D /var/lib/pgsql/9.2/svp_db/data stop

	-bash-4.1$ /usr/pgsql-9.2/bin/pg_ctl -D /var/lib/pgsql/9.2/svp_db/data stop
	waiting for server to shut down..................... done
	server stopped
	-bash-4.1$

- createdb
	postgres user("su - postgres")!!!!!!!!!!!!!!!!!!!
	/usr/pgsql-9.2/bin/createdb -E UTF8 -U postgres -T template1  -h localhost -p 6444 svpdb

	-bash-4.1$ /usr/pgsql-9.2/bin/createdb -E UTF8 -U postgres -T template1  -h localhost -p 6444 svpdb
	-bash-4.1$

Database installed and running!!!!!!


161110:
=======
Meeting - follow up of development process:
- put main config to db using Hibernate
	- Database SVP
		- Table SampleApplication
			- Config
				- Id (ApplicationInstance)
				- ApplicationName
				- ORBInitialHost
				- ORBInitialPort
				- ContextName
				- MpCcManager
				- UiManager
				- ServiceNumber
				- DebugLevel
				- ApplicationData "/opt/svp/sample/config/sample.xml"
				.
				.
				.

	spring.database.driverClassName=org.postgresql.Driver
	spring.datasource.url= jdbc:postgresql://172.24.1.144:5443/svp
	spring.datasource.username=postgres
	spring.datasource.password=postgres

- use REST Interface for attach, detach, act, info, status

- check Details of Spring Application with Christian
- Discussion about Graphical Callflow development



161104:
=======
- check logging							- OPEN!!! 
	http://www.slf4j.org/manual.html
	http://www.codingpedia.org/ama/how-to-log-in-spring-with-slf4j-and-logback/     logback.xml...
	http://logback.qos.ch/manual/configuration.html

	http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-logging.html
	http://www.slf4j.org/faq.html#configure_logging
	http://stackoverflow.com/questions/5448673/slf4j-logback-how-to-configure-loggers-in-runtime
	http://logback.qos.ch/manual/layouts.html

	https://samerabdelkafi.wordpress.com/2013/10/23/logback/

	https://www.mkyong.com/logging/logback-set-log-file-name-programmatically/


161014:
=======
- create new Project and copy VMS source to Spring Application   - O.K.

- put load and write config file to			- O.K.
	load Properties:  Properties props loadProps(String file);
	store Properties: storeProps(Properties props, String file);
	load Properties:  Hashtable<String,String> variables loadProps(String file);
	store Properties: storeProps(Hashtable<String,String> variables, String file);

		at.sonorys.svp.service.api.configApi.java
		at.sonorys.svp.service.impl.configService.java

161013:
=======
- put all values from Hashtable to SystemVariables	- O.K.
- use SystemVariables for Startup...
- with readcfg reread all SystemVariables
- check this with FrameworkService...
- put logic for parsing properties file to service.impl
			- getAllConfigValues

- how to load all properties				- O.K.
			Properties prop = new Properties();
			InputStream input = null;
			input = new FileInputStream("/opt/svp/sample/config/sample2.properties");
			prop.load(input);

- load specific application.properties			- O.K.
see this documentation:
http://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/htmlsingle/#boot-features-external-config


161012:
=======
- how to shutdown framework				- solved!!!!
http://stackoverflow.com/questions/26547532/how-to-shutdown-a-spring-boot-application-in-a-correct-way