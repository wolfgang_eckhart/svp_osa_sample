@echo off

set HOST=localhost
set PORT=24900



rem check Arguments
if ""%1""=="""" goto noCommand

set CLASSPATH=C:\opt\svp\jars\socket_comm.jar

set COMMAND=""%1""

:doneSetArgs

@echo on
java at.sonorys.util.sc.SocketComm %HOST% %PORT% %COMMAND%
goto end


:noCommand
echo ... Argument for command empty!!!
goto end

:end
